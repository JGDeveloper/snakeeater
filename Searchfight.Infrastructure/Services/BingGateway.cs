﻿using Newtonsoft.Json;
using Searchfight.ConsoleApp.API;
using Searchfight.Infrastructure.DTOs.Bing;
using Searchfight.Infrastructure.Handler;
using Searchfight.Infrastructure.Services.Abstractions;
using System;
using System.Threading.Tasks;

namespace Searchfight.Infrastructure.Services
{
    public class BingGateway : IBingGateway
    {
        private readonly Uri _baseAddress;
        private readonly string _subscriptionKey;
        private readonly string _customConfigId;
        private readonly IAppConfig _config;
        private readonly IHttpHandler _handler;

        public BingGateway(IAppConfig config, IHttpHandler handler)
        {            
            _config = config;
            _handler = handler;
            
            _baseAddress = new Uri(_config.BingSearchSettings.BaseAddress);
            _subscriptionKey = _config.BingSearchSettings.Parameters["SubscriptionKey"];
            _customConfigId = _config.BingSearchSettings.Parameters["CustomConfigId"];
        }

        public async Task<BingResponseDto> GetBingSearchByExpression(string value)
        {
            _handler.AddHeader(_subscriptionKey);

            // TODO solo usar value
            var searchTerm = value;
            
            //var query = "https://api.bing.microsoft.com/v7.0/custom/search?" + "q=" + searchTerm + "&" + "customconfig=" + _customConfigId + "&" + "mkt=en-US";

            var query = _baseAddress + "search?q=" + searchTerm + "&" + "customconfig=" + _customConfigId + "&" + "mkt=en-US";

            var response = await _handler.GetAsync(query);

            var content = response.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<BingResponseDto>(content);

            return result;
        }
    }
}
