﻿using Searchfight.Infrastructure.DTOs.Google;
using System.Threading.Tasks;

namespace Searchfight.Infrastructure.Services.Abstractions
{
    public interface IGoogleGateway
    {
        Task<GoogleResponseDto> GetGoogleSearchByExpression(string value);
    }
}
