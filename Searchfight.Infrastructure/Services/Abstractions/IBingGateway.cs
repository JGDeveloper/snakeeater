﻿using Searchfight.Infrastructure.DTOs.Bing;
using System.Threading.Tasks;

namespace Searchfight.Infrastructure.Services.Abstractions
{
    public interface IBingGateway
    {
        Task<BingResponseDto> GetBingSearchByExpression(string value);
    }
}
