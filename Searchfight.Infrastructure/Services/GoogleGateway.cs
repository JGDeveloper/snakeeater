﻿using Newtonsoft.Json;
using Searchfight.ConsoleApp.API;
using Searchfight.Infrastructure.DTOs.Google;
using Searchfight.Infrastructure.Handler;
using Searchfight.Infrastructure.Services.Abstractions;
using System;
using System.Threading.Tasks;

namespace Searchfight.Infrastructure.Services
{
    public class GoogleGateway : IGoogleGateway
    {
        private readonly Uri _baseAddress;
        private readonly string _apiKey;
        private readonly string _cx;
        private readonly IAppConfig _config;
        private readonly IHttpHandler _handler;

        public GoogleGateway(IAppConfig config, IHttpHandler handler)
        {
            _config = config;
            _handler = handler;

            _baseAddress = new Uri(_config.GoogleSearchSettings.BaseAddress);
            _apiKey = _config.GoogleSearchSettings.Parameters["ApiKey"];
            _cx = _config.GoogleSearchSettings.Parameters["CX"];
        }

        public async Task<GoogleResponseDto> GetGoogleSearchByExpression(string value)
        {
            // TODO solo usar value
            var searchTerm = value;

            //var query = "https://www.googleapis.com/customsearch/v1?" + "key=" + _apiKey + "&" + "cx=" + _cx + "&" + "q=" + searchTerm;

            var query = _baseAddress + "v1?" + "key=" + _apiKey + "&" + "cx=" + _cx + "&" + "q=" + searchTerm;

            var response = await _handler.GetAsync(query);

            var content = response.Content.ReadAsStringAsync().Result;

            var result  = JsonConvert.DeserializeObject<GoogleResponseDto>(content);

            return result;
        }
    }
}
