﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Searchfight.Infrastructure.DTOs.Google
{
    public class SearchInformationDto
    {
        public double searchTime { get; set; }

        public string totalResults { get; set; }
    }
}
