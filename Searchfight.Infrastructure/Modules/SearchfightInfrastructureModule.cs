﻿using Microsoft.Extensions.DependencyInjection;
using Searchfight.Infrastructure.Handler;
using Searchfight.Infrastructure.Services;
using Searchfight.Infrastructure.Services.Abstractions;

namespace Searchfight.Infrastructure.Modules
{
    public static class SearchfightInfrastructureModule
    {
        public static IServiceCollection RegisterInfrastructureComponents(this IServiceCollection services)
        {
            services.AddSingleton<IBingGateway, BingGateway>();
            services.AddSingleton<IGoogleGateway, GoogleGateway>();
            services.AddSingleton<IHttpHandler, HttpHandler>();
            return services;
        }
    }
}
