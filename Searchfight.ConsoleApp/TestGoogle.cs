﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Searchfight.ConsoleApp
{
    public static class TestGoogle
    {
        public static void GoogleSearchEngine()
        {
            var ApiKey = "AIzaSyByNe3RPjqJOO8pIfpRBUSKzpNrhmhuOIM";
            var cx = "2c114aa2bb1785d3a";
            var searchTerm = "developer";

            var url = "https://www.googleapis.com/customsearch/v1?" +
            "key=" + ApiKey + "&" +
            "cx=" + cx + "&" +
            "q=" + searchTerm;

            var client = new HttpClient();

            Console.WriteLine("Making request to Google API");
            var httpResponseMessage = client.GetAsync(url).Result;
            var responseContent = httpResponseMessage.Content.ReadAsStringAsync().Result;
            GoogleCustomSearchResponse response = JsonConvert.DeserializeObject<GoogleCustomSearchResponse>(responseContent);

            Console.WriteLine("Total Matches: " + response.searchInformation.searchTime);
            Console.WriteLine("Web search Url: " + response.searchInformation.totalResults);
        }

        public class GoogleCustomSearchResponse
        {
            public string kind { get; set; }

            public Queries queries { get; set; }

            public SearchInformation searchInformation { get; set; }
        }

        public class SearchInformation
        {
            public double searchTime { get; set; }
            public string totalResults { get; set; }
        }

        public class Queries
        {
            public Request[] request { get; set; }
        }

        public class Request
        {
            public string title { get; set; }
            public int totalResults { get; set; }
            public string searchTerms { get; set; }
        }

    }
}
