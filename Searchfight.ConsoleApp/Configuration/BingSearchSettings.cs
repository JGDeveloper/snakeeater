﻿using Searchfight.ConsoleApp.API;
using System.Collections.Generic;

namespace Searchfight.ConsoleApp.Configuration
{
    public class BingSearchSettings : IBingSearchSettings
    {
        public string BaseAddress { get; set; }

        public Dictionary<string, string> Parameters { get; set; }
    }
}