﻿using Microsoft.Extensions.Options;
using Searchfight.ConsoleApp.API;
using System;
using System.Collections.Generic;
using System.Text;

namespace Searchfight.ConsoleApp.Configuration
{
    public class AppConfig : IAppConfig
    {
        private readonly AppSettings _appSettings;
        private readonly GoogleSearchSettings _googleSettings;
        private readonly BingSearchSettings _bingSettings;

        public AppConfig(IOptions<AppSettings> options, IOptions<GoogleSearchSettings> googleSettings, IOptions<BingSearchSettings> bingSettings)
        {
            _appSettings = options.Value;
            _googleSettings = googleSettings.Value;
            _bingSettings = bingSettings.Value;
        }

        public string GoogleSearchEngineBaseUrl => _appSettings.GoogleSearchEngineBaseUrl;

        public string BingSearchEngineBaseUrl => _appSettings.BingSearchEngineBaseUrl;

        public IGoogleSearchSettings GoogleSearchSettings => _googleSettings;

        public IBingSearchSettings BingSearchSettings => _bingSettings;
    }
}
