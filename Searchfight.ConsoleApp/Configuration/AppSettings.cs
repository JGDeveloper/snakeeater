﻿namespace Searchfight.ConsoleApp.Configuration
{
    public class AppSettings
    {
        public string GoogleSearchEngineBaseUrl { get; set; }

        public string BingSearchEngineBaseUrl { get; set; }
    }
}
