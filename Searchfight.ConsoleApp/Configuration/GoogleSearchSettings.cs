﻿using Searchfight.ConsoleApp.API;
using System.Collections.Generic;

namespace Searchfight.ConsoleApp.Configuration
{
    public class GoogleSearchSettings : IGoogleSearchSettings
    {
        public string BaseAddress { get; set; }

        public Dictionary<string, string> Parameters { get; set; }
    }
}
