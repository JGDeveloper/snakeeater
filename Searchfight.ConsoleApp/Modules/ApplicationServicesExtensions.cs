﻿using Microsoft.Extensions.DependencyInjection;
using Searchfight.Application.Modules;
using Searchfight.ConsoleApp.API;
using Searchfight.ConsoleApp.Configuration;

namespace Searchfight.ConsoleApp.Modules
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IAppConfig, AppConfig>();
            //services.AddScoped<IGoogleSearchEngineSettings, GoogleSearchEngineSettings>();
            //services.AddScoped<IBingSearchEngineSettings, BingSearchEngineSettings>();
            services.AddScoped<SearchfightEngine>();
            services.RegisterBusinessComponents();                    
            return services;
        }
    }
}
