﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Searchfight.ConsoleApp.Configuration;
using Searchfight.ConsoleApp.Modules;
using System;

namespace Searchfight.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var host = CreateHost();
                var services = host.Services;

                // usar esta?
                var searchEngine = services.GetService<SearchfightEngine>();
                searchEngine.Execute();

                // o usar esta?
                //var searchEngine = ActivatorUtilities.CreateInstance<SearchfightSearchEngine>(services);
                //searchEngine.Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an error while running the application: ", ex);
                throw;
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static IHost CreateHost() => new HostBuilder()
            .ConfigureAppConfiguration((hostContext, config) =>
            {
                config.AddJsonFile("appsettings.json", false);
            })
            .ConfigureServices((hostContext, services) => 
            {
                services.AddOptions()
                    .Configure<AppSettings>(hostContext.Configuration.GetSection("AppSettings"))
                    .Configure<GoogleSearchSettings>(hostContext.Configuration.GetSection("GoogleSearchSettings"))
                    .Configure<BingSearchSettings>(hostContext.Configuration.GetSection("BingSearchSettings"));

                services.AddApplicationServices();
            })
            .Build();
    }
}
