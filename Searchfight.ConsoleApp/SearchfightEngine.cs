﻿using Searchfight.Application.Abstractions;

namespace Searchfight.ConsoleApp
{
    public class SearchfightEngine
    {
        private readonly ISearchEvaluator _search;

        public SearchfightEngine(ISearchEvaluator search)
        {
            _search = search;
        }

        public void Execute()
        {
            _search.EvaluateExpression("developer");
        }
    }
}
