﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Searchfight.ConsoleApp
{
    public static class TestBing
    {
        public static void BingSearchEngine()
        {
            var subscriptionKey = "386ac9437554464883a89d3f4d250631";
            var customConfigId = "a7455c58-9f30-488c-abb3-3a10f73f0383";
            var searchTerm = "developer";

            var url = "https://api.bing.microsoft.com/v7.0/custom/search?" +
            "q=" + searchTerm + "&" +
            "customconfig=" + customConfigId + "&" +
            "mkt=en-US";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

            Console.WriteLine("Making request to Bing API");
            var httpResponseMessage = client.GetAsync(url).Result;
            var responseContent = httpResponseMessage.Content.ReadAsStringAsync().Result;
            BingCustomSearchResponse response = JsonConvert.DeserializeObject<BingCustomSearchResponse>(responseContent);


            Console.WriteLine("Total Matches: " + response.webPages.totalEstimatedMatches);
            Console.WriteLine("Web search Url: " + response.webPages.webSearchUrl);

            //for (int i = 0; i < response.webPages.value.Length; i++)
            //{
            //    var webPage = response.webPages.value[i];

            //    Console.WriteLine("name: " + webPage.name);
            //    Console.WriteLine("url: " + webPage.url);
            //    Console.WriteLine("displayUrl: " + webPage.displayUrl);
            //    Console.WriteLine("snippet: " + webPage.snippet);
            //    Console.WriteLine("dateLastCrawled: " + webPage.dateLastCrawled);
            //    Console.WriteLine();
            //}

        }

    }

    public class BingCustomSearchResponse
    {
        public string _type { get; set; }
        public WebPages webPages { get; set; }
    }

    public class WebPages
    {
        public string webSearchUrl { get; set; }
        public int totalEstimatedMatches { get; set; }
        public WebPage[] value { get; set; }
    }

    public class WebPage
    {
        public string name { get; set; }
        public string url { get; set; }
        public string displayUrl { get; set; }
        public string snippet { get; set; }
        public DateTime dateLastCrawled { get; set; }
        public string cachedPageUrl { get; set; }
    }
}
