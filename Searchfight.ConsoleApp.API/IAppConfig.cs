﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Searchfight.ConsoleApp.API
{
    public interface IAppConfig
    {
        string GoogleSearchEngineBaseUrl { get; }

        string BingSearchEngineBaseUrl { get; }

        IGoogleSearchSettings GoogleSearchSettings { get; }

        IBingSearchSettings BingSearchSettings { get; }
    }
}
