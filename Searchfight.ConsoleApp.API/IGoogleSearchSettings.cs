﻿using System.Collections.Generic;

namespace Searchfight.ConsoleApp.API
{
    public interface IGoogleSearchSettings
    {
        string BaseAddress { get; set; }

        Dictionary<string, string> Parameters { get; set; }
    }
}
