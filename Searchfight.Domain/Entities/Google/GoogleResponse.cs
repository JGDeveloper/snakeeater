﻿namespace Searchfight.Domain.Entities.Google
{
    public class GoogleResponse
    {
        public string Kind { get; set; }

        public SearchInformation SearchInformation { get; set; }
    }
}
