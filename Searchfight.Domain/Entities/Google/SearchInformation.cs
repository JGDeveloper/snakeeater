﻿namespace Searchfight.Domain.Entities.Google
{
    public class SearchInformation
    {
        public double searchTime { get; set; }

        public string totalResults { get; set; }
    }
}
