﻿namespace Searchfight.Domain.Entities.Bing
{
    public class WebPages
    {
        public string WebSearchUrl { get; set; }

        public int TotalEstimatedMatches { get; set; }
    }
}
