﻿using System.Collections.Generic;

namespace Searchfight.Domain.Entities.Bing
{
    public class BingResponse
    {
        public string _Type { get; set; }

        public WebPages WebPages { get; set; }
    }
}
