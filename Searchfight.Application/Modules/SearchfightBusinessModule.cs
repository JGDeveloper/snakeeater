﻿using Microsoft.Extensions.DependencyInjection;
using Searchfight.Application.Abstractions;
using Searchfight.Infrastructure.Modules;

namespace Searchfight.Application.Modules
{
    public static class SearchfightBusinessModule
    {
        public static IServiceCollection RegisterBusinessComponents(this IServiceCollection services)
        {
            services.AddScoped<ISearchEvaluator, SearchEvaluator>();
            services.RegisterInfrastructureComponents();
            return services;
        }
    }
}
