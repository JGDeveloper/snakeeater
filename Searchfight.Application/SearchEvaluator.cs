﻿using Searchfight.Application.Abstractions;
using Searchfight.Infrastructure.Services.Abstractions;

namespace Searchfight.Application
{
    public class SearchEvaluator : ISearchEvaluator
    {
        private readonly IGoogleGateway _googleGateway;
        private readonly IBingGateway _bingGateway;

        public SearchEvaluator(IGoogleGateway googleGateway, IBingGateway bingGateway)
        {
            _googleGateway = googleGateway;
            _bingGateway = bingGateway;
        }

        public string EvaluateExpression(string value)
        {
            var googleResult = _googleGateway.GetGoogleSearchByExpression(value);
            var info = googleResult.Result.SearchInformation.totalResults;

            var bingResult = _bingGateway.GetBingSearchByExpression(value);
            var info2 = bingResult.Result.WebPages.TotalEstimatedMatches;

            return info;
        }
    }
}
