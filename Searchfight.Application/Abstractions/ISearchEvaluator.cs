﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Searchfight.Application.Abstractions
{
    public interface ISearchEvaluator
    {
        string EvaluateExpression(string value);
    }
}
